package com.xzh.httpclientInhome;


import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class HttpClientTest {

    public static void main(String[] args) {

    }

    /**
     * 无参Get请求
     */
    @Test
    public void GetWithoutParams() throws IOException {
        //创建客户端
        HttpClient httpClient = HttpClients.createDefault();
        //创建请求地址
        String url = "http://localhost:8080/woniusales/";
        HttpGet httpGet = new HttpGet(url);
        //发起请求,获取响应对象
        HttpResponse httpResponse = httpClient.execute(httpGet);
        //获取响应体
        HttpEntity entity = httpResponse.getEntity();
        //额外加的 获取相应头
        Header[] allHeaders = httpResponse.getAllHeaders();
        for (Header allHeader : allHeaders) {
            System.out.println(allHeader.toString());
        }
        //通过HTTP实体工具类，转换响应数据，使用字符集UTF-8
        String s = EntityUtils.toString(entity, "UTF-8");
        System.out.println(s);
        //
    }

    /**
     * 有普通参数的post请求---拼接url 【同样的GET有参也可以使用这种方式】
     */
    @Test
    public void postWithParams() {
        //创建客户端
        HttpClient httpClient = HttpClients.createDefault();
        //请求参数的构造
        String url = "http://localhost:8080/woniusales/user/login";
        String params = "username=admin&password=admin123&verifycode=0000";
        //同get方式将参数放在url后边
        HttpPost httpPost = new HttpPost(url + "?" + params);
        //获取请求的完整的url
        URI uri = httpPost.getURI();
        HttpResponse response;
        try {
            //获取响应对象
            response = httpClient.execute(httpPost);
            //获取响应实体对象
            HttpEntity entity = response.getEntity();
            //使用工具类将响应实体转换成字符串
            String s = EntityUtils.toString(entity);
            System.out.println("请求的url是: " + uri);
            System.out.println("响应中的内容是: " + s);
            System.out.println("响应状态是: " + response.getStatusLine());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    /**
     * 有普通参数的post请求 使用key_value的方式传入表单
     */
    @Test
    public void postWithParamsWithTextBody() {
        //创建客户端
        HttpClient httpClient = HttpClients.createDefault();
        //请求参数的构造
        String url = "http://localhost:8080/woniusales/user/login";
        //同get方式将参数放在url后边
        HttpPost httpPost = new HttpPost(url);
        HttpResponse response;
        //创建请求的表单内容
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("username", "admin"));
        params.add(new BasicNameValuePair("password", "admin123"));
        params.add(new BasicNameValuePair("verifycode", "0000"));
        //申明一个表单体
        UrlEncodedFormEntity FormEntity;
        try {
            //将表单内容放入表单体中
            FormEntity = new UrlEncodedFormEntity(params);
            //将表单体放入请求对象中
            httpPost.setEntity(FormEntity);
            //获取响应对象
            response = httpClient.execute(httpPost);
            //获取响应体对象
            HttpEntity entity = response.getEntity();
            //使用工具类将响应实体转换成字符串
            String s = EntityUtils.toString(entity);
            System.out.println("请求的url是: " + url);
            System.out.println("响应中的内容是: " + s);
            System.out.println("响应状态码 是: " + response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    /**
     * 有普通参数的post请求+上传图片
     */
    @Test
    public void postImage() {
        HttpClient httpClient = HttpClients.createDefault();
        String url = "http://132.232.46.158:8080/SHJKDemo/demo/fileUpload.do";
        HttpPost httpPost = new HttpPost(url);
        //创建文件对象
        File file = new File("E:\\data\\HttpClientTest\\src\\main\\java\\com\\xzh\\httpclientInhome\\scjt.png");
        //上传图片需要用到MultipartEntityBuilder  创建一个builder 要单独导一个jar包
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        //设置表单参数
        builder.addTextBody("funcationType", "uploadFile");
        builder.addTextBody("fileType", ".png");
        builder.addTextBody("appId", "UATCfeecbff640c34f49aefa719790afff84");
        builder.addTextBody("fileURL", "");//不能使用null 不然会报错
        builder.addTextBody("fileName1", "");
        builder.addTextBody("isIVPlatform", "");
        builder.addTextBody("idcardNo", "");
        builder.addTextBody("userId", "");
        builder.addTextBody("ecifNo", "");
        builder.addTextBody("creditExtensionNo", "");
        builder.addTextBody("borrowNo", "");
        builder.addTextBody("certificateNo", "");
        builder.addTextBody("expectSize", "");
        //设置文件参数，文件对象，文件MIME格式，文件名称
        builder.addBinaryBody("file", file, ContentType.create("image/png"), "scjt.png");
        // 利用builder返回一个请求体
        HttpEntity entity = builder.build();
        //将请求体设置在请求对象中
        httpPost.setEntity(entity);

        try {
            HttpResponse response = httpClient.execute(httpPost);
            System.out.println(response.getStatusLine());
            HttpEntity entity1 = response.getEntity();
            String s = EntityUtils.toString(entity1, StandardCharsets.UTF_8);
            System.out.println(s);


        } catch (IOException e) {
            System.out.println("报错了？");
            e.printStackTrace();
        }


    }


}
